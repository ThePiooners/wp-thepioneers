<ul class="selfer_metabox_tabs">
    <?php if( function_exists( 'register_block_type' ) ) { ?>
    <li class="active"><a href="#gutenberg"><?php echo esc_html__( "Gutenberg Options", 'selfer-core' ) ?></a></li>
	<li><a href="#header"><?php echo esc_html__( "Header", 'selfer-core' ) ?></a></li>
    <?php } else { ?>
    <li class="active"><a href="#header"><?php echo esc_html__( "Header", 'selfer-core' ) ?></a></li>
    <?php } ?>
    <li><a href="#contents"><?php echo esc_html__( "Content Area", 'selfer-core' ) ?></a></li>
	<li><a href="#footer"><?php echo esc_html__( "Footer", 'selfer-core' ) ?></a></li>
</ul>
<div class="selfer_metabox">
    <?php if( function_exists( 'register_block_type' ) ) { ?>
    <div class="selfer_metabox_tab" id="selfer_tab_gutenberg">
        <?php 
        $this->select ( 'show_page_title', esc_html__( 'Show Page Title', 'selfer-core' ), array (
            'default' => esc_html__( 'Select to display title', 'selfer-core' ),
            'yes' => esc_html__( 'Yes', 'selfer-core' ),
            'no' => esc_html__( 'No', 'selfer-core' )
        ), esc_html__( 'Choose to show or hide the page title.', 'selfer-core' ) );
        $this->select ( 'show_featured_image_in_content', esc_html__( 'Show featured image in the content', 'selfer-core' ), array (
            'default' => esc_html__( 'Select to display Featured Image', 'selfer-core' ),
            'yes' => esc_html__( 'Yes', 'selfer-core' ),
            'no' => esc_html__( 'No', 'selfer-core' )
        ), esc_html__( 'Choose no to hide featured image in the content area.', 'selfer-core' ) );
        ?>
    </div>
    <?php } ?>
    <div class="selfer_metabox_tab" id="selfer_tab_header">
        <?php
        $menus = wp_get_nav_menus();
        $menuName = array();
        foreach ( $menus as $menu  ) : 
            $menuName[0] = esc_html__( 'Default', 'selfer-core' );
            $menuName[$menu->term_id] = $menu->name;
        endforeach;
        $this->select ( 'header_show_header', esc_html__( 'Show header', 'selfer-core' ), array (
            'default'   => esc_html__( 'Theme Setting', 'selfer-core' ),
            'yes'       => esc_html__( 'Yes', 'selfer-core' ),
            'no'        => esc_html__( 'No', 'selfer-core' ),
        ), esc_html__( 'Choose to show or hide the header.', 'selfer-core' ) );

        $this->select ( 'header_menu_sticky', esc_html__( 'Enable sticky/fixed header', 'selfer-core' ), array (
            'default'   => esc_html__( 'Default', 'selfer-core' ),
            'yes'       => esc_html__( 'Yes', 'selfer-core' ),
            'no'        => esc_html__( 'No', 'selfer-core' )
        ), esc_html__( 'Choose to enable or disable sticky menu.', 'selfer-core' ) );

        $this->select ( 'header_page_menu', esc_html__( 'Select Menu For Page', 'selfer-core' ), $menuName , esc_html__( 'Show Diffrent Menu at diffrent pages.', 'selfer-core' ) );

        $this->upload ( 'header_logo_main', esc_html__( 'Header Main Logo', 'selfer-core' ),esc_html__( 'Choose Page Header Main Logo.', 'selfer-core' ) );
        /* Start Header Background Status */
        ?>
    </div>

    <div class="selfer_metabox_tab" id="selfer_tab_contents">
        <?php 
        $this->text ( 'content_padding_top',  
            esc_html__( 'Padding Top', 'selfer-core' ),  
            esc_html__( 'Leave it empty for default.', 'selfer-core' ) );

        $this->text ( 'content_padding_bottom',  
            esc_html__( 'Padding Bottom', 'selfer-core' ),  
            esc_html__( 'Leave it empty for default.', 'selfer-core' ) );
        ?>
    </div>

    <div class="selfer_metabox_tab" id="selfer_tab_footer">
        <?php
        $this->select ( 'footer_show_footer', esc_html__( 'Show footer', 'selfer-core' ), array (
            'default' => esc_html__( 'Theme Setting', 'selfer-core' ),
            'yes' => esc_html__( 'Yes', 'selfer-core' ),
            'no' => esc_html__( 'No', 'selfer-core' ),
        ), esc_html__( 'Choose to show or hide the footer.','selfer-core' ) );
        ?>
    </div>

</div>
<div class="clear"></div>