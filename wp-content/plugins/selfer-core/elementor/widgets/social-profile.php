<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
/**
 * Selfer Contact Form 7 Widget.
 *
 * Selfer widget that inserts an embeddable content into the page, from any given URL.
 *
 * @since 1.0
 */
class Selfer_Social_Profile_Widget extends Widget_Base {

	public function get_name() {
		return 'slefer-social-profile';
	}

	public function get_title() {
		return esc_html__( 'Social Url', 'selfer-core' );
	}

	public function get_icon() {
		return 'eicon-share';
	}

	public function get_categories() {
		return [ 'selfer-category' ];
	}

	/**
	 * Register Edu_Exp widget controls.
	 *
	 * @since 1.0
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'social_info_aligments',
			[
				'label' => esc_html__( 'Alignments', 'selfer-core' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);	

		$this->add_control(
			'social_item_alignment',
			[
				'label' => esc_html__( 'Alignment', 'selfer-core' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'start' => [
						'title' => esc_html__( 'Start', 'selfer-core' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'selfer-core' ),
						'icon' => 'fa fa-align-center',
					],
					'end' => [
						'title' => esc_html__( 'End', 'selfer-core' ),
						'icon' => 'fa fa-align-right',
					],			
					'around' => [
						'title' => esc_html__( 'Around', 'selfer-core' ),
						'icon' => 'fa fa-th-large',
					],		
					'between' => [
						'title' => esc_html__( 'Between', 'selfer-core' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => 'between',
				'toggle' => true,
			]
		);

		$this->end_controls_section();	

		$this->start_controls_section(
			'social_info_section',
			[
				'label' => esc_html__( 'Social Url', 'selfer-core' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'social_info_elem',
			[
				'label' => esc_html__( 'Info Item', 'selfer-core' ),
				'type' => Controls_Manager::REPEATER,  
				'fields' => [ 
					[
						'name' => 'title',
						'label' => esc_html__( 'title', 'selfer-core' ),
						'type' => Controls_Manager::TEXT,
						'label_block' => true,
						'default' => '',
					],					
					[
						'name' => 'icon',
						'label' => esc_html__( 'Icon', 'selfer-core' ),
						'type' => Controls_Manager::ICON,
						'label_block' => true,
					],
					[
						'name' => 'custom_icon',
						'label' => esc_html__( 'or Custom Icon', 'selfer-core' ),
						'label_block' => true,
						'type' => Controls_Manager::TEXTAREA,
					],
					[
						'name' => 'social_url',
						'label' => esc_html__( 'URL', 'selfer-core' ),
						'type' => Controls_Manager::URL,
						'placeholder' => esc_html__( 'https://your-link.com', 'selfer-core' ),
						'show_external' => true,
						'default' => [
							'url' => '',
							'is_external' => true,
							'nofollow' => true,
						]
					],					
				],
				'title_field' => ' {{ title }}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'info_styling',
			[
				'label' => esc_html__( 'Style', 'selfer-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'social_item_bg',
			[
				'label' => esc_html__( 'Icon Background', 'selfer-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ffffff',
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],
			]
		);		

		$this->add_control(
			'social_item_icons_color',
			[
				'label' => esc_html__( 'Icon Color', 'selfer-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ffffff',
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],
				'selectors' => [
					'{{WRAPPER}} .selfer-social-items i' => 'color: {{VALUE}};',
				],
			]
		);	

		$this->end_controls_section();
	}


	/**
	 * Render Edu_Exp widget output on the frontend.
	 *
	 * @since 1.0
	 */
	protected function render() { 
		$settings = $this->get_settings_for_display();
		if( $settings['social_item_alignment'] == 'start' ) {
			$alignment = 'justify-content-start';
		} elseif ( $settings['social_item_alignment'] == 'center' ) {
			$alignment = 'justify-content-center';
		} elseif ( $settings['social_item_alignment'] == 'end' ) {
			$alignment = 'justify-content-end';
		} elseif ( $settings['social_item_alignment'] == 'around' ) {
			$alignment = 'justify-content-around';
		} else {
			$alignment = 'justify-content-between';
		} ?>

		<div class="d-md-flex <?php echo esc_attr( $alignment ); ?> selfer-social-items">
			<?php foreach ($settings['social_info_elem'] as $key => $value) { ?>
				<?php 
					$target = $value['social_url']['is_external'] ? ' target="_blank"' : '';
					$nofollow = $value['social_url']['nofollow'] ? ' rel="nofollow"' : '';
				?>

	           	<a href="<?php echo esc_url( $value['social_url']['url'] ); ?>" class="mb-3 mr-3 d-flex text-white ts-align__vertical" <?php echo ( $target .' '. $nofollow ); ?>>
		           	<span class="ts-circle__xs border ts-border-transparent mr-4" data-bg-color="<?php echo esc_attr( $settings['social_item_bg'] ) ?>">
		           	    <i class="<?php echo esc_attr( ( $value['custom_icon'] !=='' ) ? $value['custom_icon']  : $value['icon']  ); ?>"></i>
		           	</span>
	           	    <span class="social-title"><?php echo esc_attr( $value['title'] ); ?></span>
	           	</a>
	           	<!--end link-->
	        <?php } ?> 
       </div>
		<?php 
	}

	/**
	 * Render Social Info widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _content_template() {
		?>
		<#
			if( settings.social_item_alignment == 'start' ) {
				var alignment = 'justify-content-start';
			} else if ( settings.social_item_alignment == 'center' ) {
				var alignment = 'justify-content-center';
			} else if ( settings.social_item_alignment == 'end' ) {
				var alignment = 'justify-content-end';
			} else if ( settings.social_item_alignment == 'around' ) {
				var alignment = 'justify-content-around';
			} else {
				var alignment = 'justify-content-between';
			}
		#>
		<div class="d-md-flex {{ alignment }} selfer-social-items">
			<# _.each( settings.social_info_elem, function( value ) { #>
				<# 
					var target = value.social_url.is_external ? ' target="_blank"' : '';
					var nofollow = value.social_url.nofollow ? ' rel="nofollow"' : '';
				#>
	           	<a href="{{{ value.social_url.url }}}" class="mb-3 mr-3 d-flex text-white ts-align__vertical" {{ target }} {{ nofollow }}>
		           	<span class="ts-circle__xs border ts-border-transparent mr-4" data-bg-color="{{ settings.social_item_bg }}">
		           	    <i class="{{ ( value.custom_icon !=='' ) ? value.custom_icon : value.icon }}"></i>
		           	</span>
	           	    <span class="social-title">{{{ value.title }}}</span>
	           	</a>
			<# }); #>
		</div>
		
		<?php
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new Selfer_Social_Profile_Widget() );